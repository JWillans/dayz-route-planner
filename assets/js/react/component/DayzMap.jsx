import '../../../scss/react/component/DayzMap.scss';

import React from 'react';

import {Map, TileLayer, Marker, Polyline} from 'leaflet';
import Leaflet from "leaflet";
import type {City, Loot, LootEntity, LootEntityType, LootObject, LootPosition, Route, RouteNode} from "../types";
import {LatLng} from "leaflet/dist/leaflet-src.esm";

const tileDefinitions = {
    'namalsk': {
        'max_zoom': 7,
        'center': [50, -20],
        'label': 'Namalsk',
        'url_template': `https://maps.izurvive.com/maps/Namalsk-Top/0.1.0/tiles/{z}/{x}/{-y}.png`,
        'cities_endpoint': '/data/namalsk/cities.json',
        'loot_endpoint': '/data/namalsk/loot.json',
    },
    'chernarus': {
        'max_zoom': 7,
        'center': null,
        'label': 'Chernarus',
        'url_template': `https://maps.izurvive.com/maps/ChernarusPlus-Top/1.0.0/tiles/{z}/{x}/{y}.webp`,
        'cities_endpoint': null,
        'loot_endpoint': null,
    }
}

const mapDefaultOptions = {
    center: [0, 0],
    zoom: 2,
}

function createLeafletTileLayer(mapName: string): TileLayer
{
    const def = tileDefinitions[mapName];
    return Leaflet.tileLayer(def.url_template, {
        attribution: 'https://dayz.ginfo.gg',
        minZoom: 1,
        maxZoom: 8,
    });
}

function createPlaceNameMarker(name: string, lat: number, lng: number): Marker
{
    return Leaflet.marker([lat, lng], {
        icon: Leaflet.divIcon({
            html: `<div class="dayz-map--map--place-name-marker--name">${name}</div>`,
            className: 'dayz-map--map--place-name-marker',
            iconSize: [0, 0],
            iconAnchor: [0, 0]
        }),
        zIndexOffset: 1000,
    });
}

function createCircleMarker(colour: string, lat: string, lng: string): Marker
{
    return Leaflet.marker([lat, lng], {
        icon: Leaflet.divIcon({
            html: `<div class="dayz-map--map--circle-marker--circle" style="background-color: ${colour};"></div>`,
            className: 'dayz-map--map--circle-marker',
            iconSize: [8, 8],
            iconAnchor: [4, 4]
        })
    });
}

function createIconMarker(src: string, lat: string, lng: string): Marker
{
    return Leaflet.marker([lat, lng], {
        icon: Leaflet.divIcon({
            html: `<img class="dayz-map--map--icon-marker--icon" src="${src}"/>`,
            className: 'dayz-map--map--icon-marker',
            iconSize: [16, 16],
            iconAnchor: [8, 8]
        })
    });
}

function encodeRoute(route: Route): ?string
{
    if(route.length === 0) return null;

    let parts = [];
    route.forEach((node: RouteNode) => {
        parts.push(Math.round(node.lat * 100) / 100);
        parts.push(Math.round(node.lng * 100) / 100);
    });

    let coordsStr = parts.join(',');
    return `route:${coordsStr}`;
}

function decodeRoute(url: string): Route
{
    let match = `${url}`.match(/#route:(.+)/);
    if(match === null) return [];

    let route = [];

    let coordsStr = match[1];
    let parts = coordsStr.split(',').map((part: string) => {
        return parseFloat(part);
    });

    while(parts.length > 0){
        route.push({
            lat: parts.shift(),
            lng: parts.shift(),
        });
    }

    console.log(route);

    return route;
}

export function mountLeafletMap(mapName: string, container: HTMLElement, options: {}|undefined): Map
{
    options = {
        ...mapDefaultOptions,
        ...(options ?? {})
    }

    const map = Leaflet.map(container, options);
    map.addLayer(createLeafletTileLayer(mapName));
    return map;
}

type DayzMapProps = {
    mapName: string,
    configureMap: (map: Map) => void | null,
}

type DayzMapState = {
    mapName: string,
    cities: City[]|null,
    loot: Loot|null,
    route: Route,
}

function resolveLootObjectIconPath(entity: LootEntity, type: LootEntityType, obj: LootObject): ?string
{
    const baseUrl = '/images/icons';

    if(entity.name === 'Medical'){
        if(obj.name.includes('office')){
            return baseUrl + `/loot_icon_medical_hospital.svg`;
        }
        return baseUrl + `/loot_icon_medical_office.svg`;
    }

    if(entity.name === 'Military'){

        if(type.name === 'Generic') return baseUrl + `/loot_icon_military_generic.svg`;
        if(type.name === 'Barracks') return baseUrl + `/loot_icon_military_barracks.svg`;
        if(type.name === 'GuardTower') return baseUrl + `/loot_icon_military_guardtower.svg`;
        if(type.name === 'Tent') return baseUrl + `/loot_icon_military_tent.svg`;
        if(obj.name.includes('guardhouse')) return baseUrl + `/loot_icon_military_guardhouse.svg`;
    }

    return null;
}

export default class DayzMap extends React.Component<DayzMapProps, DayzMapState>
{

    static defaultProps = {
        mapName: 'namalsk',
        configureMap: null,
    }

    /**
     * @type {null|Map}
     */
    map = null;

    /**
     * @type {Marker[]}
     */
    markers = [];

    /**
     * @type {Polyline|null}
     */
    routeLine = null;

    constructor(props) {
        super(props);

        this.state = {
            mapName: this.props.mapName,
            cities: null,
            route: decodeRoute(window.location.href),
        }

        this.mapRef = React.createRef();
    }

    componentDidMount()
    {
        this.initMap();
    }

    loadCities()
    {
        const url = tileDefinitions[this.state.mapName].cities_endpoint;
        if(!url) return;

        fetch(url)
            .then((response) => response.json())
            .then((cities: City[]) => {
                this.setState({cities: cities}, () => {
                    this.redrawMarkers();
                });
            })
            .catch(console.error)
        ;
    }

    addRouteNode(node: RouteNode)
    {
        this.setState({route: [...this.state.route, node]}, () => {
            this.redrawRoute();
            this.updateUrlRoute();
        });
    }

    redrawRoute()
    {
        if(this.routeLine !== null){
            this.routeLine.removeFrom(this.map);
        }

        if(this.state.route.length === 0) return;

        const latLngs = this.state.route.map((node: RouteNode) => {
            return new LatLng(node.lat, node.lng);
        })

        this.routeLine = Leaflet.polyline(latLngs, {
            color: 'yellow',
            weight: 3,
            opacity: 1,
            smoothFactor: 1,
        });

        this.routeLine.addTo(this.map);
    }

    updateUrlRoute()
    {
        const url = `${window.location.pathname}#${encodeRoute(this.state.route) ?? ''}`;
        window.history.replaceState(null, document.title, url);
    }

    loadLoot()
    {
        const url = tileDefinitions[this.state.mapName].loot_endpoint;
        if(!url) return;

        fetch(url)
            .then((response) => response.json())
            .then((loot: Loot) => {
                this.setState({loot: loot}, () => {
                    this.redrawMarkers();
                });
            })
            .catch(console.error)
        ;
    }

    clearMarkers()
    {
        this.markers.forEach((marker: Marker) => {
            marker.removeFrom(this.map);
        });
        this.markers = [];
    }

    redrawMarkers()
    {
        this.clearMarkers();

        const zoom = this.map.getZoom();

        (this.state.cities ?? []).forEach((city: City) => {
            if(zoom < city.minZoom) return;
            const marker = createPlaceNameMarker(city.nameRU ?? city.nameEN, city.lat, city.lng);
            marker.addTo(this.map);
            this.markers.push(marker);
        });

        /**
         * @type {Loot}
         */
        const loot = this.state.loot;

        if(loot !== null){

           loot.static.forEach((entity: LootEntity) => {

               entity.types.forEach((type: LootEntityType) => {

                   type.objects.forEach((obj: LootObject) => {

                       obj.positions.forEach((position: LootPosition) => {

                           if(zoom < position[2]) return; // Min-zoom level
                           const iconPath = resolveLootObjectIconPath(entity, type, obj);
                           if(iconPath === null) return;
                           const marker = createIconMarker(iconPath, position[0], position[1]);
                           marker.addTo(this.map);
                           this.markers.push(marker);
                       })

                   })

               });
           })

        }
    }

    componentDidUpdate(prevProps: DayzMapProps, prevState: DayzMapState)
    {
        if(this.state.mapName !== prevState.mapName){
            this.initMap();
        }
    }

    initMap()
    {
        if(this.map !== null){
            this.map.remove();
            this.map = null;
        }

        const options = {
            center: tileDefinitions[this.state.mapName].center ?? [0, 0],
            zoom: 2,
            maxZoom: tileDefinitions[this.state.mapName].max_zoom,
            minZoom: 2,
        };

        this.map = mountLeafletMap(this.state.mapName, this.mapRef.current, options);
        if(this.props.configureMap !== null) this.props.configureMap(this.map);

        this.map.on('zoomend', () => {
            this.redrawMarkers();
        })

        this.map.on('click', (e) => {
            const latLng = e.latlng;
            const node = {lat: latLng.lat, lng: latLng.lng};
            this.addRouteNode(node);
        })

        this.setState({
            cities: null,
            loot: null,
        }, () => {
            this.loadCities();
            this.loadLoot();
        })

        this.redrawRoute();
    }

    renderControlMapSelect()
    {
        const onChange = (e: InputEvent) => {
            this.setState({mapName: e.currentTarget.value});
        }

        return (
            <select name="map_name" id="map_name" onChange={onChange}>
                {Object.keys(tileDefinitions).map((mapName: string) =>
                    <option key={mapName} value={mapName}>{tileDefinitions[mapName].label}</option>
                )}
            </select>
        )
    }

    renderControlClearRoute()
    {
        const onClick = (e: MouseEvent) => {
            this.setState({route: []}, () => {
                this.redrawRoute();
                this.updateUrlRoute();
            });
        }

        return (
            <button type="button" onClick={onClick}>Clear</button>
        )
    }

    renderControlUndoRouteNode()
    {
        const onClick = (e: MouseEvent) => {
            e.preventDefault();
            let route = [...this.state.route];
            route.pop();
            if(route.length <= 1) route = [];
            this.setState({route: route}, () => {
                this.redrawRoute();
                this.updateUrlRoute();
            });
        }

        return (
            <button type="button" onClick={onClick}>Undo</button>
        )
    }

    render()
    {
        return (
            <div className="dayz-map">

                <div className="dayz-map--controls">
                    {this.renderControlMapSelect()}
                    {this.renderControlUndoRouteNode()}
                    {this.renderControlClearRoute()}
                </div>

                <div className="dayz-map--map" ref={this.mapRef} />
            </div>
        )
    }

}
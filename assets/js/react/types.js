export type City = {
    nameEN: string,
    nameRU: string,
    lat: number,
    lng: number,
    spellings: string[],
    type: string,
    minZoom: number,
}

export type Loot = {
    static: LootEntity[],
    events: LootEntity[],
    animals: LootEntity[],
    nature: LootEntity[],
    areas: LootEntity[],
    config: {
        tiers: LootConfigTier[]
    }
}

export type LootEntityType = {
    name: string,
    zIndex: number,
    primaryUsage: string,
    objects: LootObject[],
}

export type LootPosition = [
    number, // Lat
    number, // Lng
    number, // Min Zoom
]

export type LootObject = {
    name: string,
    displayName: string,
    usages: string[],
    categories: string[],
    positions: LootPosition[],
}

export type LootEntity = {
    name: string,
    displaySortOrder: number,
    types: LootEntityType[],
}

export type LootConfigTier = {
    tier: number,
    color: string,
}

export type RouteNode = {
    lat: number,
    lng: number,
}

export type Route = RouteNode[];

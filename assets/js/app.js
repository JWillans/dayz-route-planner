import '../scss/app.scss';

import {initAll} from './react/init';
import {resolveComponent} from "./react/map";

initAll(resolveComponent);